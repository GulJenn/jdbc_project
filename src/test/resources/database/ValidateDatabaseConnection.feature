Feature: Validate connection

  @dataBase
  Scenario Outline: Validating the Database Connection
    Given User is able to connect to database
    When User send the "<query>" to database
    And Employees expected first and last names

      | Neena   | Kochhar   |
      | Lex     | De Haan   |
      | Den     | Raphaely  |
      | Matthew | Weiss     |
      | Adam    | Fripp     |
      | Payam   | Kaufling  |
      | Shanta  | Vollman   |
      | Kevin   | Mourgos   |
      | John    | Russell   |
      | Karen   | Partners  |
      | Alberto | Errazuriz |
      | Gerald  | Cambrault |
      | Eleni   | Zlotkey   |
      | Michael | Hartstein |

    Examples:
      | query                                                                                                                    |
      | SELECT first_name ,last_name from employees where manager_id=(SELECT manager_id from employees where first_name='Payam') |