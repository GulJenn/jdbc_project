package database;

import org.testng.Assert;

import java.sql.*;

public class DatabaseConnection {
    public static void main(String[] args) throws SQLException {

        String url = "jdbc:oracle:thin:@batch4db1.cup7q3kvh5as.us-east-2.rds.amazonaws.com:1521/ORCL";
        String username = "guluzar";
        String password = "guluzar123!";
        String query = "select first_name, last_name from employees where manager_id = (select manager_id from employees where first_name='Payam')";

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

        while (resultSet.next()) {

            String firstName = resultSet.getString("FIRST_NAME"); // FIRST_NAME is the column name
//            System.out.println("First name of the employees: " + firstName);
            String lastName = resultSet.getString("LAST_NAME"); // LAST_NAME is the column name
//            System.out.println("Last name of the employees: " + lastName);
            System.out.println(firstName + "                          " + lastName);

            // Validating the data from the table
            if (firstName.equals("David") && lastName.equals("Austin")) {
                String actualName = firstName;
                Assert.assertEquals(actualName, "David");
                System.out.println("Actual name: " + firstName + "  " + lastName);
                break;
            }
        }
    }


    }
